module.exports = function(grunt) {
  grunt.initConfig({
    browserify: {
      development: {
        src: ['./main.js'],
        dest: './dist/bundle.js',
        options: {
          watch: true,
          keepAlive: true,
          browserifyOptions: { debug: true },
          transform: [['babelify', { presets: ['es2015'] }]]
        }
      }
    }
  });
  grunt.loadNpmTasks('grunt-browserify');
};
